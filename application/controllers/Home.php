<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public $cat_table;
	public $prod_table;
	public function __construct()
	{
		parent::__construct();

		$this->prod_table = "vi03_products";
		$this->cat_table = "vi02_product_category";

		$this->load->model('dashboard_model', 'DashboardModel');
	}

	public function index()
	{
		//Get Products
		$select  = [
			$this->prod_table.'.id',
			$this->prod_table.'.name',
			$this->prod_table.'.sku',
			$this->prod_table.'.description',
			$this->prod_table.'.price',
			$this->prod_table.'.qty',
			$this->prod_table.'.image',
			$this->cat_table.'.cate_name',
		];
		$join = [
			[
				"table" => $this->cat_table,
				"where" => $this->prod_table . '.cat_id = ' . $this->cat_table . '.id'
			]
		];
		$where = [
			$this->cat_table . '.is_deleted' => '0',
			$this->cat_table . '.status' => '1',
			$this->prod_table . '.is_deleted' => '0',
			$this->prod_table . '.status' => '1'
		];
		$data['products'] = $this->DashboardModel->getAllJoin($select,$this->prod_table,$where,$join);
		$data['category'] = $this->DashboardModel->getAll($this->cat_table, ['is_deleted' => '0']);

		$this->load->view('header');
		$this->load->view('home',$data);
		$this->load->view('footer');
	}

	public function productDetails($id){
		//Get Products
		$select  = [
			$this->prod_table.'.id',
			$this->prod_table.'.name',
			$this->prod_table.'.sku',
			$this->prod_table.'.description',
			$this->prod_table.'.price',
			$this->prod_table.'.qty',
			$this->prod_table.'.image',
			$this->cat_table.'.cate_name',
		];
		$join = [
			[
				"table" => $this->cat_table,
				"where" => $this->prod_table . '.cat_id = ' . $this->cat_table . '.id'
			]
		];
		$where = [
			$this->cat_table . '.is_deleted' => '0',
			$this->cat_table . '.status' => '1',
			$this->prod_table . '.is_deleted' => '0',
			$this->prod_table . '.status' => '1',
			$this->prod_table . '.id' => $id
		];
		$data['product'] = $this->DashboardModel->getAllJoin($select,$this->prod_table,$where,$join);
		$this->load->view('header');
		$this->load->view('productdetail',$data);
		$this->load->view('footer');
	}
}
