<?php
class Dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // Select all Query
    public function getAll($table,$where){
        $query =  $this->db->get_where($table,$where);
        return $query->result_array();
    }

    // Select all with join Query
    public function getAllJoin($select=[],$table,$where,$joins=[]){
        if(!empty($select)){
            if(count($select) > 1){
                $this->db->select(implode(",",$select));
            }else{
                $this->db->select($select[0]);
            }
        }else{
            $this->db->select("*");
        }
        $this->db->from($table);
        if(!empty($joins)){
            foreach($joins as $join){
                $this->db->join($join['table'],$join['where']);
            }
        }
        $this->db->where($where);
        $query =  $this->db->get();
        return $query->result_array();
    }

    // Select one query
    public function getWhere($table,$where){
        $query = $this->db->get_where($table,$where);
        return $query->row_array();
    }
}