-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: violet
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vi01_admin`
--

DROP TABLE IF EXISTS `vi01_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vi01_admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `profile_img` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint DEFAULT '1' COMMENT '0-in active,1-active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='This is admin user table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vi01_admin`
--

LOCK TABLES `vi01_admin` WRITE;
/*!40000 ALTER TABLE `vi01_admin` DISABLE KEYS */;
INSERT INTO `vi01_admin` VALUES (1,'admin','admin@yopmail.com','e6e061838856bf47e1de730719fb2609',NULL,'2021-09-13 20:50:54','2021-09-13 20:50:54',1);
/*!40000 ALTER TABLE `vi01_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vi02_product_category`
--

DROP TABLE IF EXISTS `vi02_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vi02_product_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(100) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint DEFAULT '1' COMMENT '0-in active,1-active',
  `is_deleted` enum('0','1') DEFAULT '0' COMMENT '0-not delete,1-deleted',
  PRIMARY KEY (`id`),
  KEY `created by` (`created_by`),
  KEY `updated by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='product category manage';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vi02_product_category`
--

LOCK TABLES `vi02_product_category` WRITE;
/*!40000 ALTER TABLE `vi02_product_category` DISABLE KEYS */;
INSERT INTO `vi02_product_category` VALUES (1,'Dresses',1,1,'2021-09-13 22:32:39','2021-09-14 00:59:34',1,'0'),(2,'Bags ee',1,1,'2021-09-13 22:46:03','2021-09-13 22:59:40',1,'1'),(3,'FootWare',1,1,'2021-09-13 23:00:52','2021-09-13 23:00:52',1,'0');
/*!40000 ALTER TABLE `vi02_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vi03_products`
--

DROP TABLE IF EXISTS `vi03_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vi03_products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cat_id` int DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `description` longtext,
  `price` decimal(10,2) DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint DEFAULT '1' COMMENT '0-in active,1-active',
  `is_deleted` enum('0','1') DEFAULT '0' COMMENT '0-not delete,1-deleted',
  PRIMARY KEY (`id`),
  KEY `category` (`cat_id`),
  KEY `created by` (`created_by`),
  KEY `updated by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='products manage tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vi03_products`
--

LOCK TABLES `vi03_products` WRITE;
/*!40000 ALTER TABLE `vi03_products` DISABLE KEYS */;
INSERT INTO `vi03_products` VALUES (1,1,'One piece bodysuit','OPB01','One piece bodysuit',50.00,4,'d1c86160a8815a0d91381c4db5081e6f.jpg',1,1,'2021-09-13 23:50:00','2021-09-14 00:27:34',1,'0'),(2,1,'Dotted Blue Shirt','DBS056','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.',22.90,6,'79640804f25b193f6922ef430a839428.jpg',1,1,'2021-09-14 00:13:43','2021-09-14 00:27:13',1,'0'),(3,3,'Yellow Maxi Dress','YMD036','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.',25.90,7,'706198ff4a4c5ed4fb53684743e2474c.jpg',1,1,'2021-09-14 01:02:07','2021-09-14 01:02:07',1,'0');
/*!40000 ALTER TABLE `vi03_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-14  1:26:20
