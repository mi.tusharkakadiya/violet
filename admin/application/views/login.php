<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Focus - Bootstrap Admin Dashboard </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo ASSETS_PATH;?>images/favicon.png">
    <link href="<?php echo ASSETS_PATH;?>css/style.css" rel="stylesheet">
    <link href="<?php echo ASSETS_PATH;?>css/custom.css" rel="stylesheet">

</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                <?php  echo $this->session->flashdata('message'); ?>
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form form-validation">
                                    <h4 class="text-center mb-4">Sign in your account</h4>
                                    <form class="form-valide" action="<?php echo BASE_PATH.'login';?>" method="post">
                                        <div class="form-group">
                                            <label for="val-email"><strong>Email</strong> <span class="text-danger">*</span></label>
                                            <input type="email" id="val-email" name="email" class="form-control">
                                            <?php echo form_error('email');?>
                                        </div>
                                        <div class="form-group">
                                            <label for="val-password"><strong>Password</strong> <span class="text-danger">*</span></label>
                                            <input type="password" name="password" id="val-password" class="form-control" >
                                            <?php echo form_error('password');?>
                                        </div>
                                        <!-- <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                                <div class="form-check ml-2">
                                                    <input class="form-check-input" type="checkbox" id="basic_checkbox_1">
                                                    <label class="form-check-label" for="basic_checkbox_1">Remember me</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="page-forgot-password.html">Forgot Password?</a>
                                            </div>
                                        </div> -->
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Sign me in</button>
                                        </div>
                                    </form>
                                    <!-- <div class="new-account mt-3">
                                        <p>Don't have an account? <a class="text-primary" href="./page-register.html">Sign up</a></p>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="<?php echo ASSETS_PATH;?>vendor/global/global.min.js"></script>
    <script src="<?php echo ASSETS_PATH;?>js/quixnav-init.js"></script>
    <script src="<?php echo ASSETS_PATH;?>js/custom.min.js"></script>
    
    <!-- Jquery Validation -->
    <script src="<?= ASSETS_PATH;?>vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- Form validate init -->
    <script src="<?= ASSETS_PATH;?>js/plugins-init/jquery.validate-init.js"></script>
</body>

</html>