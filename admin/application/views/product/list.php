<link href="<?php echo ASSETS_PATH; ?>vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">

<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <!-- <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Hi, welcome back!</h4>
                    <span class="ml-1">Datatable</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Table</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Datatable</a></li>
                </ol>
            </div>
        </div> -->
        <!-- row -->


        <div class="row">
            <div class="col-12">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Product</h4>
                        <a href="<?php echo BASE_PATH . 'product/addproduct'; ?>" class="btn btn-primary">Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Name</th>
                                        <th>Sku</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($products)) {
                                        $i = 1;
                                        foreach ($products as $product) {
                                    ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $product['name']; ?></td>
                                                <td><?php echo $product['sku']; ?></td>
                                                <td><?php echo $product['price']; ?></td>
                                                <td><?php echo $product['qty']; ?></td>
                                                <td><?php echo ($product['status'] == '1' ? "Active" : "In Active"); ?></td>
                                                <td>
                                                    <a href="<?php echo BASE_PATH . 'product/editproduct/' . $product['id']; ?>"><i class="fa fa-edit fa-lg"></i></a>
                                                    <a href="<?php echo BASE_PATH . 'product/deleteproduct/' . $product['id']; ?>"><i class="fa fa-trash fa-lg"></i></a>
                                                </td>
                                            </tr>
                                        <?php $i++;
                                        }
                                    } else { ?>
                                        <tr>
                                            <td colspan="7"><?php echo DATA_EMPTY; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Name</th>
                                        <th>Sku</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
            Content body end
        ***********************************-->

<!-- Datatable -->
<script src="<?php echo ASSETS_PATH; ?>vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/plugins-init/datatables.init.js"></script>