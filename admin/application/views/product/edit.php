<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Product</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="post" enctype="multipart/form-data" action="<?php echo BASE_PATH . 'product/editproduct/' . $product['id']; ?>">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" class="form-control" id="sel1">
                                        <option value="">Select Category</option>
                                        <?php
                                        foreach ($category as $cate) {
                                            if ($product['cat_id'] == $cate['id']) {
                                                $selected = "selected";
                                            } else {
                                                $selected = "";
                                            }
                                        ?>
                                            <option value="<?php echo $cate['id']; ?>" <?php echo $selected; ?>><?php echo $cate['cate_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('category'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control input-default " value="<?php echo $product['name']; ?>" placeholder="Product Name">
                                    <?php echo form_error('name'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Sku</label>
                                    <input type="text" name="sku" class="form-control input-default " value="<?php echo $product['sku']; ?>" placeholder="Sku">
                                    <?php echo form_error('sku'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" value="<?php echo $product['description']; ?>" rows="4" id="comment"><?php echo $product['description']; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity" min="0" value="<?php echo $product['qty']; ?>" class="form-control input-default" placeholder="Quantity">
                                    <?php echo form_error('quantity'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" name="price" value="<?php echo $product['price']; ?>" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text">.00</span>
                                        </div>
                                    </div>
                                    <?php echo form_error('price'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" name="featureimg" class="custom-file-input">
                                            <label class="custom-file-label">Choose file</label>
                                        </div>
                                    </div>
                                    <?php if ($product['image'] != null) { ?>
                                        <img src="<?php echo PRODUCT_IMAGE_PATH . $product['image']; ?>" height="100" width="100" />
                                    <?php } ?>
                                </div>
                                <div class="form-group d-flex flex-row-reverse">
                                    <button type="submit" class="m-1 btn btn-primary">Update</button>
                                    <a href="<?php echo BASE_PATH . 'product'; ?>" class="m-1 btn btn-dark">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>