<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->session->flashdata('message'); ?>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Category</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="post" action="<?php echo BASE_PATH . 'category/editcategory/' . $category['id']; ?>">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="cate_name" class="form-control input-default" value="<?php echo $category['cate_name']; ?>" placeholder="Category Name..">
                                    <?php echo form_error('cate_name'); ?>
                                </div>
                                <div class="form-group d-flex flex-row-reverse">
                                    <button type="submit" class="m-1 btn btn-primary">Update</button>
                                    <a href="<?php echo BASE_PATH . 'category'; ?>" class="m-1 btn btn-dark">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>