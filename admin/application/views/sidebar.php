 <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
                    <li>
                        <a href="<?php echo BASE_PATH;?>" aria-expanded="false">
                            <i class="icon icon-single-04"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="icon icon-single-04"></i><span class="nav-text">Product Manage</span></a>
                        <ul aria-expanded="false">
                            <li><a href="<?php echo BASE_PATH.'category';?>">Category</a></li>
                            <li><a href="<?php echo BASE_PATH.'product';?>">Product</a></li>
                        </ul>
                    </li>
                </ul>
            </div>


        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->
