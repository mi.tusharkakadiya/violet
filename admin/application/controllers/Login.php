<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model','AdminModel');
    }

    public function index(){
        if($this->session->has_userdata('admin_logged_in')){
			redirect('dashboard');
        }
        $this->form_validation->set_error_delimiters('<div class="form-error">', '</div>');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE){
            $this->load->view('login');
            return FALSE;
        }else{
            $loginArr =  array(
                                "email"=>$this->input->post('email'),
                                "password"=>md5($this->input->post('password')),
                                "status"=>ACTIVE
                            );
            $output = $this->AdminModel->getAdminDetail($loginArr);
            if(isset($output) && !empty($output)){
                $session_data=array(
                    'id'=>$output['id'],
                    'username'=>$output['username'],
                    'email'=>$output['email']
                );
                $this->session->set_userdata('admin_logged_in', $session_data);
                redirect('dashboard');
            }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>'.LOGIN_FAILED.'
                                                        </div>');
                redirect('login');
            }
        }
    }

    public function signout(){
		$this->session->unset_userdata('admin_logged_in');
		redirect('login');
	}

}