<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public $session_result;
    public $cat_table;
    public $prod_table;
	public function __construct()
    {
        parent::__construct();
		if (!$this->session->has_userdata('admin_logged_in')) {
            redirect('login');
        }
        $this->prod_table = "vi03_products";
        $this->cat_table = "vi02_product_category";
        $this->session_result = $this->session->userdata('admin_logged_in');
		$this->load->model('admin_model','AdminModel');
		$this->load->model('product_model','ProductModel');
	}

    public function index(){
        $sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);
        $data['products'] = $this->ProductModel->getAll($this->prod_table, ['is_deleted' => '0']);

		$this->load->view('header',$data);
		$this->load->view('sidebar');
		$this->load->view('product/list');
		$this->load->view('footer');
    }

    // Add Product
    public function addProduct(){
        $sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);
        $data['category'] = $this->ProductModel->getAll($this->cat_table, ['is_deleted' => '0']);

        $this->form_validation->set_error_delimiters('<div class="form-error">', '</div>');
        $this->form_validation->set_rules('category', 'Product Category', 'trim|required');
        $this->form_validation->set_rules('name', 'Product Name', 'trim|required');
        $this->form_validation->set_rules('sku', 'Product Sku', 'trim|required');
        $this->form_validation->set_rules('quantity', 'Product Quantity', 'trim|required|numeric');
        $this->form_validation->set_rules('price', 'Product Price', 'trim|required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header',$data);
            $this->load->view('sidebar');
            $this->load->view('product/add');
            $this->load->view('footer');
            return false;
        }else{
            // echo "<pre>";print_r($_POST);echo "</pre>";
            // echo "<pre>";print_r($_FILES);echo "</pre>";
            $data = [
                "cat_id"=>$this->input->post('category'),
                "name"=>$this->input->post('name'),
                "sku"=>$this->input->post('sku'),
                "description"=>$this->input->post('description'),
                "price"=>$this->input->post('price'),
                "qty"=>$this->input->post('quantity'),
                "created_by"=>$sessId,
                "updated_by"=>$sessId
            ];
            if(isset($_FILES['featureimg']) && !empty($_FILES['featureimg']['name'])){
                // Checking whether file exists or not
                if (!is_dir(PRODUCT_IMAGE_UPLOAD_PATH)) {
                    // Create a new file or direcotry
                    mkdir(PRODUCT_IMAGE_UPLOAD_PATH, 0777, TRUE);
                }
                $config['upload_path'] = PRODUCT_IMAGE_UPLOAD_PATH;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_size'] = 2000;
                $config['max_width'] = 1500;
                $config['max_height'] = 1500;
                $this->upload->initialize($config);
                //$this->upload->do_upload('admin_profile');
                if(!$this->upload->do_upload('featureimg')){
                    $error = $this->upload->display_errors('<div class="alert alert-success solid alert-dismissible fade show">
                    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>','</div>');
                    $this->session->set_flashdata('message',$error);
                    redirect('product/addproduct');
                    return false;
                }else{
                    $photoInfo = $this->upload->data();
                    $data['image']=$photoInfo['file_name'];
                }
            }
            $res = $this->ProductModel->insert($this->prod_table,$data);
            if($res == true){
                $this->session->set_flashdata('message', '<div class="alert alert-success solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . PRODUCT_ADD_SUCCESS . '
                                                        </div>');
                redirect('product');
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . PRODUCT_ADD_FAILED . '
                                                        </div>');
                redirect('product/addproduct');
            }
        }
    }

    // Edit Product
    public function editProduct($id){
        $sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);
        $data['category'] = $this->ProductModel->getAll($this->cat_table, ['is_deleted' => '0']);
        $data['product'] = $this->ProductModel->getWhere($this->prod_table, ['id'=>$id,'is_deleted' => '0']);

        $this->form_validation->set_error_delimiters('<div class="form-error">', '</div>');
        $this->form_validation->set_rules('category', 'Product Category', 'trim|required');
        $this->form_validation->set_rules('name', 'Product Name', 'trim|required');
        $this->form_validation->set_rules('sku', 'Product Sku', 'trim|required');
        $this->form_validation->set_rules('quantity', 'Product Quantity', 'trim|required|numeric');
        $this->form_validation->set_rules('price', 'Product Price', 'trim|required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header',$data);
            $this->load->view('sidebar');
            $this->load->view('product/edit');
            $this->load->view('footer');
            return false;
        }else{
            $data = [
                "cat_id"=>$this->input->post('category'),
                "name"=>$this->input->post('name'),
                "sku"=>$this->input->post('sku'),
                "description"=>$this->input->post('description'),
                "price"=>$this->input->post('price'),
                "qty"=>$this->input->post('quantity'),
                "updated_by"=>$sessId
            ];
            if(isset($_FILES['featureimg']) && !empty($_FILES['featureimg']['name'])){
                // Checking whether file exists or not
                if (!is_dir(PRODUCT_IMAGE_UPLOAD_PATH)) {
                    // Create a new file or direcotry
                    mkdir(PRODUCT_IMAGE_UPLOAD_PATH, 0777, TRUE);
                }
                $config['upload_path'] = PRODUCT_IMAGE_UPLOAD_PATH;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_size'] = 2000;
                $config['max_width'] = 1500;
                $config['max_height'] = 1500;
                $this->upload->initialize($config);
                //$this->upload->do_upload('admin_profile');
                if(!$this->upload->do_upload('featureimg')){
                    $error = $this->upload->display_errors('<div class="alert alert-success solid alert-dismissible fade show">
                    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>','</div>');
                    $this->session->set_flashdata('message',$error);
                    redirect('product/editproduct/'.$id);
                    return false;
                }else{
                    $photoInfo = $this->upload->data();
                    $data['image']=$photoInfo['file_name'];
                }
            }
            $where = ["id"=>$id];
            $res = $this->ProductModel->update($this->prod_table,$where,$data);
            if($res == true){
                $this->session->set_flashdata('message', '<div class="alert alert-success solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . PRODUCT_UPDATE_SUCCESS . '
                                                        </div>');
                redirect('product');
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . PRODUCT_UPDATE_FAILED . '
                                                        </div>');
                redirect('product/editproduct/'.$id);
            }
        }
    }

    // Delete Product
    public function deleteProduct($id)
    {
        $sessId = $this->session_result['id'];
        $data = [
            "is_deleted" => '1',
            "updated_by" => $sessId
        ];
        $where = ["id" => $id];
        $res = $this->ProductModel->update($this->prod_table, $where, $data);
        if ($res == true) {
            $this->session->set_flashdata('message', '<div class="alert alert-success solid alert-dismissible fade show">
                                                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                        </button>' . PRODUCT_DELETE_SUCCESS . '
                                                    </div>');
            redirect('product');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger solid alert-dismissible fade show">
                                                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                        </button>' . PRODUCT_DELETE_FAILED . '
                                                    </div>');
            redirect('product');
        }
    }
}