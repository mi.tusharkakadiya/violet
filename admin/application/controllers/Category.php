<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

    public $session_result;
    public $cat_table;
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('admin_logged_in')) {
            redirect('login');
        }
        $this->cat_table = "vi02_product_category";
        $this->session_result = $this->session->userdata('admin_logged_in');
        $this->load->model('admin_model', 'AdminModel');
        $this->load->model('product_model', 'ProductModel');
    }

    public function index()
    {

        $sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);
        $data['category'] = $this->ProductModel->getAll($this->cat_table, ['is_deleted' => '0']);

        $this->load->view('header', $data);
        $this->load->view('sidebar');
        $this->load->view('category/list');
        $this->load->view('footer');
    }

    // Add Category
    public function addCategory()
    {
        $sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);

        $this->form_validation->set_error_delimiters('<div class="form-error">', '</div>');
        $this->form_validation->set_rules('cate_name', 'Category Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $data);
            $this->load->view('sidebar');
            $this->load->view('category/add');
            $this->load->view('footer');
            return FALSE;
        } else {
            $data = [
                "cate_name" => $this->input->post('cate_name'),
                "created_by" => $sessId,
                "updated_by"=> $sessId
            ];
            $res = $this->ProductModel->insert($this->cat_table, $data);
            if ($res == true) {
                $this->session->set_flashdata('message', '<div class="alert alert-success solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . CATE_ADD_SUCCESS . '
                                                        </div>');
                redirect('category');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . CATE_ADD_FAILED . '
                                                        </div>');
                redirect('category/addcategory');
            }
        }
    }

    // Edit Category
    public function editCategory($id)
    {
        $sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);
        $data['category'] = $this->ProductModel->getWhere($this->cat_table, ['id' => $id, 'is_deleted' => '0']);

        $this->form_validation->set_error_delimiters('<div class="form-error">', '</div>');
        $this->form_validation->set_rules('cate_name', 'Category Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $data);
            $this->load->view('sidebar');
            $this->load->view('category/edit');
            $this->load->view('footer');
            return FALSE;
        } else {
            $data = [
                "cate_name" => $this->input->post('cate_name'),
                "updated_by" => $sessId
            ];
            $where = ["id" => $id];
            $res = $this->ProductModel->update($this->cat_table, $where, $data);
            if ($res == true) {
                $this->session->set_flashdata('message', '<div class="alert alert-success solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . CATE_UPDATE_SUCCESS . '
                                                        </div>');
                redirect('category');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger solid alert-dismissible fade show">
                                                            <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                            </button>' . CATE_UPDATE_FAILED . '
                                                        </div>');
                redirect('category/editcategory/' . $id);
            }
        }
    }

    // Delete Category
    public function deleteCategory($id)
    {
        $sessId = $this->session_result['id'];
        $data = [
            "is_deleted" => '1',
            "updated_by" => $sessId
        ];
        $where = ["id" => $id];
        $res = $this->ProductModel->update($this->cat_table, $where, $data);
        if ($res == true) {
            $this->session->set_flashdata('message', '<div class="alert alert-success solid alert-dismissible fade show">
                                                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                        </button>' . CATE_DELETE_SUCCESS . '
                                                    </div>');
            redirect('category');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger solid alert-dismissible fade show">
                                                        <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                        </button>' . CATE_DELETE_FAILED . '
                                                    </div>');
            redirect('category');
        }
    }
}
