<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public $session_result;
	public function __construct()
    {
        parent::__construct();
		if (!$this->session->has_userdata('admin_logged_in')) {
            redirect('login');
        }
        $this->session_result = $this->session->userdata('admin_logged_in');
		$this->load->model('admin_model','AdminModel');
	}

	public function index()
	{
		$sessId = $this->session_result['id'];
        $data['admin'] = $this->AdminModel->getadmindetail(['id' => $sessId]);

		$this->load->view('header',$data);
		$this->load->view('sidebar');
		$this->load->view('dashboard');
		$this->load->view('footer');
	}
}
