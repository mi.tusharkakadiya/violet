<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display All Messages
|--------------------------------------------------------------------------
|
| This is for all error or success message handle file.
|
*/

defined('ACTIVE')       or define('ACTIVE', 1);
defined('DELETED')      or define('DELETED', 3);

//Global Message
defined('DATA_EMPTY')            or define('DATA_EMPTY', 'No records found.');

//Admin Login Messages
defined('LOGIN_SUCCESS')            or define('LOGIN_SUCCESS', 'User is login successfully.');
defined('LOGIN_FAILED')             or define('LOGIN_FAILED', 'Invalid email or password.');
defined('LOGIN_INACTIVE')           or define('LOGIN_INACTIVE', 'Your account is currently inactive mode.');
defined('LOGIN_SUSPEND')            or define('LOGIN_SUSPEND', 'Your account is currently suspend.');
defined('LOGIN_DELETED')            or define('LOGIN_DELETED', 'Not Available this account.');

//Category Messages
defined('CATE_ADD_SUCCESS')            or define('CATE_ADD_SUCCESS', 'Category is add successfully.');
defined('CATE_ADD_FAILED')             or define('CATE_ADD_FAILED', 'Something wenr wrong, please try again.');
defined('CATE_UPDATE_SUCCESS')            or define('CATE_UPDATE_SUCCESS', 'Category is update successfully.');
defined('CATE_UPDATE_FAILED')             or define('CATE_UPDATE_FAILED', 'Something wenr wrong, please try again.');
defined('CATE_DELETE_SUCCESS')            or define('CATE_DELETE_SUCCESS', 'Category is delete successfully.');
defined('CATE_DELETE_FAILED')             or define('CATE_DELETE_FAILED', 'Something wenr wrong, please try again.');

// Product Messages
defined('PRODUCT_ADD_SUCCESS')            or define('PRODUCT_ADD_SUCCESS', 'Product is add successfully.');
defined('PRODUCT_ADD_FAILED')             or define('PRODUCT_ADD_FAILED', 'Something wenr wrong, please try again.');
defined('PRODUCT_UPDATE_SUCCESS')            or define('PRODUCT_UPDATE_SUCCESS', 'Product is update successfully.');
defined('PRODUCT_UPDATE_FAILED')             or define('PRODUCT_UPDATE_FAILED', 'Something wenr wrong, please try again.');
defined('PRODUCT_DELETE_SUCCESS')            or define('PRODUCT_DELETE_SUCCESS', 'Product is delete successfully.');
defined('PRODUCT_DELETE_FAILED')             or define('PRODUCT_DELETE_FAILED', 'Something wenr wrong, please try again.');
