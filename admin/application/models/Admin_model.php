<?php
class Admin_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAdminDetail($data = array()){
        $this->db->select("*");
        $this->db->from("vi01_admin");
        $this->db->where($data);
        $query = $this->db->get();
		if ($query->num_rows() == 0){
			return false;
		}else{	
			return $query->row_array();
		} 
    }

}