<?php
class Product_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // Insert query
    public function insert($table,$data){
        if($this->db->insert($table,$data)){
            return true;
        }else{
            return false;
        }
    }

    // Update Query
    public function update($table,$where,$data){
        $this->db->where($where);
        if($this->db->update($table,$data)){
            return true;
        }else{
            return false;
        }
    }

    // Select all Query
    public function getAll($table,$where){
        $query =  $this->db->get_where($table,$where);
        return $query->result_array();
    }

    // Select one query
    public function getWhere($table,$where){
        $query = $this->db->get_where($table,$where);
        return $query->row_array();
    }

    // Select all with join Query
    public function getAllJoin($select=[],$table,$where,$joins=[]){
        if(!empty($select)){
            if(count($select) > 1){
                $this->db->select(implode(",",$select));
            }else{
                $this->db->select($select[0]);
            }
        }else{
            $this->db->select("*");
        }
        $this->db->from($table);
        if(!empty($joins)){
            foreach($joins as $join){
                $this->db->join($join['table'],$join['where']);
            }
        }
        $this->db->where($where);
        $query =  $this->db->get();
        return $query->result_array();
    }
}